<?php
    //deklarasi array input, deret1, dan deret 2.
    $deret1 = array();
    $deret2 = array();
    $input = readline("input panjang deret : ");

    //mencari nilai dari deret kelipatan 3 dikurang 1
    echo "Deret kelipatan 3 dikurang 1\t: ";
    for($i = 0; $i < $input ; $i++){
        if($i == 0){
            $deret1[$i] = 3 - 1; 
        }else{
            $deret1[$i] = (($i + 1) * 3) - 1;
        }
        echo $deret1[$i]. " ";
        
    }
    
    //mencari nilai dari deret kelipatan (-2) * 1
    echo "\n";
    echo "Deret kelipatan (-2) * 1\t: "; 
    for($i = 0; $i < $input; $i++){
        if($i == 0 ){
            $deret2[$i] = (-2) * 1;
        }else{
            $deret2[$i] = ((-2) * ($i + 1)) * 1;
        }
        echo $deret2[$i]. " ";
    }

    //menjumlahkan nilai indek ganjil dan genap dari kedua deret 
    //sama juga menjumlahkan indeks yang sama dari kedua deret
    echo "\n";
    echo "Hasil Penjumlahan\t\t: ";
    for($i = 0; $i < $input; $i++){
        $hasil = $deret1[$i] + $deret2[$i];
        echo $hasil. " ";
    }

?>